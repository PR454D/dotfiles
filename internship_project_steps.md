# Steps
-------------------

1. import dataset into spark
```python
df_csv = spark.read.format('csv').options(inferSchema = True, header = True).load('/path/to/file')
df_csv.createOrReplaceTempView('dataframe')
```
    - Total records: 50909729
    - Columns
    ```
            +--------------+
        |      col_name|
        +--------------+
        |          type|
        |            id|
        |  subreddit.id|
        |subreddit.name|
        |subreddit.nsfw|
        |   created_utc|
        |     permalink|
        |          body|
        |     sentiment|
        |         score|
        +--------------+

    ```
    - Export data with
    ```python
        df = spark.read.load("examples/src/main/resources/people.csv",
                     format="csv", sep=":", inferSchema="true", header="true")
        df = spark.read.load("examples/src/main/resources/users.parquet")
        df.select("name", "favorite_color").write.save("namesAndFavColors.parquet")
    ```

2. feature engineer your DS no. of columns rows

3. create & analyse temporary table
