#!/usr/bin/env bash


box () {
	title=" $1 "
	edge=$(echo "$title" | sed 's/./*/g')
	echo "$edge"
	echo -e "\e[1;34m$title\e[0m"
	echo "$edge"
}

box "$*"
