
all: blackArch dots

git-crypt:
	ls

tex:
	stow texmf
	git clone https://github.com/liantze/AltaCV ~/tmp
	cp ~/tmp/altacv.cls ~/texmf/tex/latex/altacv/
	cd ~/
	rm -rdf ~/tmp

packages:
	awk '{print $1}' packages.txt | xargs paru -S --needed
	echo "################----------DONE---------######################"
	paru -S ls ffmpegthumbnailer imagemagick poppler zathura-pdf-poppler epub-thumnailer wkhtmltopdf bat chafa catdoc docx2txt gnumeric
	git clone "https://github.com/cirala/lfimg" /opt/lfimg
	cd /opt/lfimg
	sudo make install

dots:
	/usr/bin/ls | xargs stow

docker:
	sudo pacman -Syy docker docker-compose
	sudo systemctl enable --now docker
	sudo usermod -aG docker $(USER)

blackArch:
	curl -O https://blackarch.org/strap.sh
	chmod +x strap.sh
	sudo ./strap.sh
	rm ./strap.sh


tmux:
	stow tmux
	tmux -c send-keys ';I'

jellyfin:
	sudo pacman -Sy --needed --noconfirm chaotic-aur/jellyfin
	sudo systemctl enable --now jellyfin.service

go:
	sudo pacman -Sy go
	echo 'export PATH=/opt/apache-spark/bin:/home/prasad/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/local/go/bin' >>~/.zshrc
