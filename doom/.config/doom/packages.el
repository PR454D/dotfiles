;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.

(package! tree-sitter)
(package! tree-sitter-langs)
(package! rainbow-mode)

(package! plantuml-mode)
(package! lsp-java)
(package! dap-mode)
; (package! lsp-dart)
(package! org-super-agenda)
(package! org-tree-slide)
(package! org-tree-slide-pauses)
(unpin! org-roam)
(unpin! writeroom-mode)
(package! lsp-javacomp)
(package! ox-pandoc)
(package! ox-html5slide)
(package! ox-reveal)
(package! fancy-battery)
(package! ivy-file-preview)
(package! org-superstar)
(package! svelte-mode)
